#!/usr/bin/env python3

# clip_list_generator.py - Reads the emotion and neutral ordering csv files
# and generates a file of complete file names.

emotion_ordering = []
with open('Emotion_ordering.csv', 'r') as f:
    emotion_ordering = f.readlines()
    emotion_ordering.pop(0)

neutral_ordering = []
with open('Neutral_ordering.csv', 'r') as f:
    neutral_ordering = f.readlines()
    neutral_ordering.pop(0)

emotion_filenames = []
neutral_filenames = []
for i in range(len(emotion_ordering)):
    clip_set = '-0' + str(i%3 + 1)
    list1 = emotion_ordering[i].strip('\n').split(',')
    emotion_filename = ','.join('0' + e + (clip_set + '.mp4') for e in list1)
    emotion_filenames.append(emotion_filename)

    list2 = neutral_ordering[i].strip('\n').split(',')
    neutral_filename = ','.join('01-0' + e + '.mp4' for e in list2)
    neutral_filenames.append(neutral_filename)

with open('emotion_clips.csv', 'a') as f:
    f.write('E1,E2,E3,E4,E5,E6\n')
    for line in emotion_filenames:
        f.write(line + '\n')

with open('neutral_clips.csv', 'a') as f:
    f.write('N1,N2,N3,N4,N5,N6\n')
    for line in neutral_filenames:
        f.write(line + '\n')

