#!/usr/bin/env python3

# running-times.py - 

import os
import subprocess
import re

def get_running_time(filename):
    result = subprocess.run(['ffprobe', '-v', 'error', '-show_entries', 
                             'format=duration', '-of',
                             'default=noprint_wrappers=1:nokey=1', filename],
                             stdout=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
    return float(result.stdout)

clip_dir = '../induction-clips/'
all_clips = []
clip_set1 = []
clip_set2 = []
clip_set3 = []
for filename in os.listdir(clip_dir):
    all_clips.append(filename)
    if re.match('^0[2-9]-01\.mp4$', filename): clip_set1.append(filename)
    elif re.match('^0[2-9]-02\.mp4$', filename): clip_set2.append(filename)
    elif re.match('^0[2-9]-03\.mp4$', filename): clip_set3.append(filename)

all_clips.sort()
clip_set1.sort()
clip_set2.sort()
clip_set3.sort()

set1_runtime = 0
set2_runtime = 0
set3_runtime = 0

for clip in all_clips: print(clip)

print('\nSet 1')
for clip in clip_set1: 
    runtime = get_running_time(clip_dir + clip)
    print(clip, str(runtime))
    set1_runtime += runtime
print('Total:   ', str(set1_runtime))

print('\nSet 2')
for clip in clip_set2: 
    runtime = get_running_time(clip_dir + clip)
    print(clip, str(runtime))
    set2_runtime += runtime
print('Total:   ', str(set2_runtime))

print('\nSet 3')
for clip in clip_set3: 
    runtime = get_running_time(clip_dir + clip)
    print(clip, str(runtime))
    set3_runtime += runtime
print('Total:   ', str(set3_runtime))
