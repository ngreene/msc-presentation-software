#!/usr/bin/env python3

# file_io.py - Contains the FileIO class which manages filename retrival and 
# saving feedback data.

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

import os

clip_dir = 'induction-clips/'

class FileIO:

    def __init__(self, participant_id_int : int):
        '''Initialise FileIO 

        participant_id - the unique id that represents the current participant
        emotive_set - the emotive movie set the current participant will watch
                      (there are three sets labeled as 1, 2, and 3)
        '''
        self.participant_id =str(participant_id_int).zfill(2)

        emotive_clip_list = []
        with open('util/emotion_clips.csv') as f:
            for i, line in enumerate(f):
                if i == participant_id_int:
                    lst = line.strip('\n').split(',')
                    emotive_clip_list = [clip_dir + e for e in lst]

        neutral_clip_list = []
        with open('util/neutral_clips.csv') as f:
            for i, line in enumerate(f):
                if i == participant_id_int:
                    lst = line.strip('\n').split(',')
                    neutral_clip_list = [clip_dir + e for e in lst]

        # Interleave file names for neutral and emotive clips like so:
        # [neutral, emotive, ..., neutral, emotive]
        self.clip_list = [clip for pair in zip(neutral_clip_list,
                          emotive_clip_list) for clip in pair]

        # print(emotive_clip_list)
        # print(neutral_clip_list)
        # print(self.clip_list)

        self.clip_list.insert(0, clip_dir + 'training.mp4')
        self._clip_index = 1 # clip index ignores training clip
        
        self.save_dir = 'saved/'
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)
        
        self.save_fname = self.save_dir + self.participant_id + '.csv'
        with open(self.save_fname, 'w') as f:
            f.write('filename,emotion,valence,arousal\n')

        self.max_strong_exp = 0
        self.save_exp_fname = self.save_dir + self.participant_id + '_exp.csv'
        with open(self.save_exp_fname, 'w') as f:
            f.write('filename\n')

    def save_feedback(self, rating, valence, arousal, strong_exp):
        '''Appends participant feedback to a csv file after each trial

        rating - The discrete emotion feedback
        valence - The valence feedback
        arousal - The arousal feedback
        strong_exp - A list of strong experience key presses
        '''
        valence = str(valence)
        arousal = str(arousal)
        
        # Get filename while excluding directory name
        fname = self.clip_list[self._clip_index][len(clip_dir):]
        self.clip_list[self._clip_index] = fname
        self._clip_index += 1
            
        with open(self.save_fname, 'a') as f:
            new_line = (fname + ',' + rating + ',' + valence + ',' 
                        + arousal + '\n')
            f.write(new_line)

        s_exp = ''
        if strong_exp:
            s_exp = ',' + ','.join(str(e) for e in strong_exp)

            # rewrite the first line if it needs to be longer
            if self.max_strong_exp < len(strong_exp)//2:  
                with open (self.save_exp_fname, 'r') as fr:
                    data = fr.readlines()
                    
                first_line = 'filename'
                for i in range(len(strong_exp)//2):
                    first_line += ',' + 'intense_' + str(i+1)
                    first_line += ',' + 'intense_' + str(i+1) + '_dt'
                first_line += '\n' 
                data[0] = first_line
                
                with open(self.save_exp_fname, 'w') as fw:
                    fw.writelines(data)

                self.max_strong_exp = len(strong_exp)//2


        with open(self.save_exp_fname, 'a') as f:
            new_line = (fname + s_exp + '\n')
            f.write(new_line)

    def write_file(self, fname, wr):
        with open(self.save_dir + self.participant_id + '_' + fname, 'w') as f:
            f.write(wr)
            
