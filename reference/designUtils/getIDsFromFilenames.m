function [d, dd] = getIDsFromFilenames(d)
%GETIDSFROMFILENAMES	Get trial IDs from file list
%
%   Extract IDs from *original* file naming convention.
%
%   See also: getMovFiles.m

%	Author Steven R. Livingstone
%	Version 1.0 
%   2013-01-06


d = regexp(d,'([0-9]+-)+[0-9]+','match');
dd = [d{:}]';
% Returns cell of cells containing '01 '01 '01' '01' '01'
d = regexp([d{:}],'\w+','match');
% Converts to numerical array
d = cellfun(@(x) cellfun(@str2num,x), d,'UniformOutput',false);
% Reshapes to appropriate size
d = reshape(cell2mat(d),[size(d{1},2) length(d)])';

end % end function