function [ files ] = getFilenamesFromIDs( ids )
%GETFILENAMESFROMIDS	Get new RAVDESS filenames from ids
%   FILES=GETFILENAMESFROMIDS(IDS)

tmp = reshape2d3d(ids);
wavidx = tmp(:,1) > 2;
mp4idx = tmp(:,1) < 3;
files = num2strcell(reshape2d3d(ids));
files = strcat('0',files);
files = cellfun(@(x) x(end-1:end),files,'UniformOutput',false);
files(:,1:6) = strcat(files(:,1:6),'-');
files(mp4idx,7) = strcat(files(mp4idx,7),'.mp4');
files(wavidx,7) = strcat(files(wavidx,7),'.wav');
% Concatenate the rows together
files = num2cell(files,1);
files = strcat(files{:});
% Reshape back into original size
files = reshape2d3d(files,size(ids,3));

end

