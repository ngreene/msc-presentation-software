%RAVDESSVALIDATE	Validation software for RAVDESS
%
%   Iteratively presents videos from RAVDESS. Collect feedback, store in a
%   separate logfile for each participant. Autosaves logs, allows for
%   exiting and resuming of participant testing.

%	Copyright 2013
%	Author Steven R. Livingstone
%
%	Version 1.0
%	2013-11-07

clear
setpathsRavdessValidate
Screen('Preference', 'SkipSyncTests', 1);

% Current directory of script
currDir = regexprep(mfilename('fullpath'),'ravValidate','');
vars_ravdessValidate

% Prompt user for subject number
[sub, logObj, filenames.log] = promptSubjectNumber(dirs, maxSubjects, filenames.master, sampleFiles);

% check for Opengl compatibility, abort otherwise:
AssertOpenGL;

% Make sure keyboard mapping is the same on all supported operating systems
% Apple MacOS/X, MS-Windows and GNU/Linux:
KbName('UnifyKeyNames');

% Wait until user releases keys on keyboard:
KbReleaseWait;

% Accepted feedback keyboard responses
[logkeys, logvals] = getAllowableKeys(logObj.IV_ResponseOrder);

% Abort program early flag
abortProgram = false;

% Select screen for display of movie:
screenid = max(Screen('Screens'));

 
% Embed main PTB code within try-catch block.
try
    % Hide mouse cursor on screen
    HideCursor;
    
    % Suppress all keyboard output to Matlab windows/panes during program.
    % I.e., prevents participant input appearing in editor window, breaking
    % script. See - http://psychtoolbox.org/BugReports
    ListenChar(2);
    
    % Redisplay mouse cursor
    %ShowCursor;
    % Reenable keyboard
    %ListenChar(0);
    
    % ('OpenWindow', [screenID], [Colour], [x-left y-left x-right y-right], [default pixel size], ...
    % [default num buffers], [default monoscopic monitor setup], [4x anti-aliasing])
    %[win, rect] = Screen('OpenWindow', screenid, [0 0 0], [50 50 1920-50 1080-50], [], 2, 0, 4);
    %[win, rect] = Screen('OpenWindow', screenid, [0 0 0], [50 50 600 1200], [], 2, 0, 4);
    [win, rect] = Screen('OpenWindow', screenid, [0 0 0], [], [], 2, 0, 4);
    
    % Load relevant images into memory.
    dispScreens = loadFeedbackImgs(dispimgs, win, logObj.IV_ResponseOrder);
    
    % Get rectange to display movie frame texture in.
    [movRect, screenCentre] = getMovRect(rect, mov.res);
    
    % Force system to use GStreamer as movie player engine
    Screen('Preference', 'OverrideMultimediaEngine', 1);
    
    % Open audio device.
    pahandle = PsychPortAudio('Open', [], [], 0, wav.freq, wav.nrchannels);

    
    % Setup font for text display
    Screen('TextSize', win, 60); %48
    % Change font to Arial
    Screen('TextFont', win, 'Cambria'); % Times New Roman, Garamond, Arial, sans-serif, Courier
    % 0=normal,1=bold,2=italic,4=underline,8=outline,32=condense,64=extend
    Screen('TextStyle', win, 0);
    
    % Speech/song start-block message flags
    blockMsgsDisplayed = [false, false];
    
    % Show instructions on screen
    showInstructions(win, screenCentre, instimgs);
    
    % Start real experiment clock
    tic;

    % Show 3 random practice trials
    [abortProgram] = playSampleTrials(logObj, win, movRect, pahandle, screenCentre, dispScreens, logkeys, logvals, abortProgram);
    % Abort program (if required), else update log
    if abortProgram
        % Gracefully quit program (early abort)
        stopProgram(logObj, dirs, filenames.log, abortProgram, pahandle);
        return;
    end
        
    % Loop over every stimulus in the logObj file list, play it, get feedback
    for stimCnt = logObj.counter:length(logObj.IV_files)
        
        % Get file type (1 speech, 2 song) from logObj
        stimType = logObj.IV_fileIDs(stimCnt,2);
        
        % Display message for start of a new speech or song block
        if ~blockMsgsDisplayed(stimType)
            
            % Display block message (song or speech)
            displayMessage(win, blockMsgs{stimType}, msgDispTime);
            
            % Set flag to true
            blockMsgsDisplayed(stimType) = true;
        end
        
        % Play stimulus (song or speech)
        playStimulus(logObj, dirs, win, movRect, pahandle);
        
        % Shows feedback screens (category, intensity) and capture keyboard feedback
        [keylog, keyIndexes, respTimes, abortProgram] = showFeedback(win, ...
            screenCentre, dispScreens, logkeys, logvals, abortProgram, stimType);
        
        % Update display
        Screen('Flip', win);
        
        % Abort program (if required), else update log
        if abortProgram
            % Gracefully quit program (early abort)
            stopProgram(logObj, dirs, filenames.log, abortProgram, pahandle);
            return;
        end
        
        % Update log
        logObj = updateLogObj(logObj, keylog, keyIndexes, respTimes);
        
        % Autosave log every 5th trial.
        if mod(stimCnt,5) == 0
            autosaveLog(logObj, dirs, filenames.log);
        end
    end
    
    % Gracefully quit program (program complete)
    stopProgram(logObj, dirs, filenames.log, abortProgram, pahandle);
    
catch err
    sca
    % Alert operator to a serious program error
    fprintf('\n\t ****** &&&&& SERIOUS CRASH &&&&& ******\n\t Please contact Steven and leave Matlab as it is\n.');
    
    % Gracefully quit program (early abort), save out the log.
    stopProgram(logObj, filenames.log, abortProgram, pahandle);
    
    % Re-throw error
    rethrow(err)
end