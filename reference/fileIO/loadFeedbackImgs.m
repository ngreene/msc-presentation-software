function imgTextures = loadFeedbackImgs(dispimgs,win,responseOrder)
%LOADFEEDBACKIMGS	Creates PTB textures of feedback screens for quick display

%	Author Steven R. Livingstone
%	Version 1.0 
%   2013-11-12

% Feedback
if 1 == responseOrder
    i1 = imread(dispimgs{1}{1});
    i2 = imread(dispimgs{2}{1});
elseif 2 == responseOrder
    i1 = imread(dispimgs{1}{2});
    i2 = imread(dispimgs{2}{2});
else
    sca
   error('Invalid response order: %d',responseOrder);
end
i3 = imread(dispimgs{3});
i4 = imread(dispimgs{4});
imgTextures = {Screen('MakeTexture', win, i1), Screen('MakeTexture', win, i2), ...
    Screen('MakeTexture', win, i3), Screen('MakeTexture', win, i4)};

end % end loadFeedbackImgs