function autosaveLog(logObj, dirs, logFilename)
%AUTOSAVELOG   Autosaves the log object to separate directory.
%
%   Autosaves LOG object to LOGFILE filename.

%	Author Steven R. Livingstone
%	Version 1.0 
%   2013-11-13

% Save autosave logObj
save(fullfile(dirs.autosave, logFilename), 'logObj');

end % end function