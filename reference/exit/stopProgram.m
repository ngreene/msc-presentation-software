function stopProgram(logObj, dirs, filenames, abortProgram, pahandle)
%STOPPROGRAM   Graceful abort and stopping of PD training software. 
%
%   Updates weeklyObj and saves it and autosave to disk.

%	Copyright 2013
%	Author Steven R. Livingstone
%	Version 1.0
%	2013-03-06

% End time of experiment
logObj.expEnd = toc;

% Set weekly log status to 'finished' (1) if program not aborted early
if ~abortProgram
    logObj.status = 1;
end

if ~abortProgram
    % Save log
    save(fullfile(dirs.log, filenames), 'logObj');
end

% Save autosave backup
save(fullfile(dirs.autosave, filenames), 'logObj');

% PTB graceful quit
%
% Close the audio device
PsychPortAudio('Close', pahandle);
% Redisplay mouse cursor
ShowCursor;
% Reenable keyboard
ListenChar(0);

% Close all screens
sca;

if abortProgram
    fprintf('\n\t ******* Participant quit experiment *******\n');
else
    fprintf('\n\t ******* Experiment complete! *******\n');
end

end % end function