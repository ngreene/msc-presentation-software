%VARS_RAVDESSVALIDATE	Variable declaration for ravdessValidate.m
%
%

%	Copyright 2013
%	Author Steven R. Livingstone
%	Version 1.0
%	2013-11-07

% Maximum number of subjects that can take part in validation
maxSubjects = 450;

% Duration (in seconds) to display text message on screen
msgDispTime = 3;

% Contains all the stimulus files
dirs.movie = fullfile(currDir,'inputMovies');
% Output directory for participant logs
dirs.log = fullfile(currDir,'logfiles');
% Directory containing autosave files for participants
dirs.autosave = fullfile(dirs.log,'autosave');
% Directory containing program input files
dirs.inputFiles = fullfile(currDir, 'inputFiles');
dirs.images = fullfile(currDir, 'images');
dirs.sampleFiles = fullfile(currDir, 'inputSampleMovies');

% Resolution of RAVDESS videos
mov.res = [1280 720];

% Movie file format
mov.format = '.mp4';

% Wave file format
wav.format = '.wav';

% Frequency rate of wav file
wav.freq = 48000;

% Number of channels in the wav file. It should be 1 (mono).
wav.nrchannels = 1;

% Block messages (Domain, Modality)
blockMsgs = {'You will now be shown: Speech recordings','You will now be shown: Song recordings'};

% Master list of files for each subject
filenames.master = 'masterLog.mat';

dispimgs = {{fullfile(dirs.images,'1-1.png'), fullfile(dirs.images,'1-2.png')}, ...
    {fullfile(dirs.images,'2-1.png'), fullfile(dirs.images,'2-2.png')}, fullfile(dirs.images,'3.png'), fullfile(dirs.images,'4.png')};

instimgs = {fullfile(dirs.images,'Inst1.png'), fullfile(dirs.images,'Inst2.png'), fullfile(dirs.images,'Inst3.png'), fullfile(dirs.images,'Inst4.png')};

sampleFiles = {{fullfile(dirs.sampleFiles, '1-1.mp4'), fullfile(dirs.sampleFiles, '1-2.mp4'), fullfile(dirs.sampleFiles, '1-3.wav')}, ...
    {fullfile(dirs.sampleFiles, '2-1.mp4'), fullfile(dirs.sampleFiles, '2-2.mp4'), fullfile(dirs.sampleFiles, '2-3.wav')}, ...
    {fullfile(dirs.sampleFiles, '3-1.mp4'), fullfile(dirs.sampleFiles, '3-2.mp4'), fullfile(dirs.sampleFiles, '3-3.wav')}, ...
    {fullfile(dirs.sampleFiles, '4-1.mp4'), fullfile(dirs.sampleFiles, '4-2.mp4'), fullfile(dirs.sampleFiles, '4-3.wav')}};