function [sub, logObj, logfilename] = promptSubjectNumber(dirs, maxSubjects, masterFilename, sampleFiles)
%PROMPTSUBJECTNUMBER	Prompts operator for valid subject number.
%
%   Prompts user for their subject number. Function loops and checks input
%   until allowable & desired info has been entered by operator.
%
%   See also: promptWeekNumber.m

%	Copyright 2013
%	Author Steven R. Livingstone
%	Version 1.0
%	2013-03-02
%   Version 1.1
%   2013-11-07

% Input messages for subject number and week number.
% First entry is pc message, second is mac
if ispc
    subjectMsg = 'Enter the participant''s ID: (ctrl-c to quit): ';
elseif ismac
    subjectMsg = 'Enter the participant''s ID: (apple-c to quit): ';
end

% Loop prompt until a valid, new subject number has been entered
while true
    % Get subject number
    [sub.str, sub.num] = getValidInput(subjectMsg, maxSubjects, 'Participant');
    
    % Potential logfile name for this subject
    logfilename = strcat(sub.str,'.mat');
    
    % Get the list of subject's weekly session log files
    filelist = dirfiles('.mat', dirs.log, 0)';
    
    % Check if a log file for this subject exists already.
    %
    % NOTE - This is only a partial check, as we split subjects between
    % machines.
    logexists = ismember(logfilename,filelist);
    
    % Check if final logfile exists (only created if program finished).
    if logexists
        % Presence of a logfile means subject is complete
        fprintf('That participant number has already completed the experiment. Please check the number printed on the participant''s questionnaire form.\n');
    else
        % No log file, so check if autosave exists
        % Check if autosave log exists (incomplete subject)
        %
        % Get the list of autosave logfiles
        autolist = dirfiles('.mat', dirs.autosave, 0)';
        autoexists = ismember(logfilename,autolist);
        if autoexists
            fprintf('Loading incomplete logfile for subject: %d\n', sub.num);
            % Load autosave log
            load(fullfile(dirs.autosave,logfilename));
        else
            % Create a new log object
            logObj = createNewLog(dirs, sub, masterFilename, logfilename, sampleFiles);
        end
        % Escape the while loop
        break
    end
    
end

end % end promptSubjectNumber
