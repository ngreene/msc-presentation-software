function [strInput, numInput] = getValidInput(inputMsg, numLimit, promptType)
%GETBALIDINPUT	Ensures a valid prompt input has been provided

%	Copyright 2013
%	Author Steven R. Livingstone
%	Version 1.0
%	2013-11-09

while true
    % Get subject or week number from user
    strInput = input(inputMsg,'s');
    
    % Ask to confirm input is correct
    inputConf = confirmInput('Are you sure [y/n]? ', {'y','n'});
    
    % Restart prompt loop if a 'no' is entered.
    if isequal(inputConf,'n')
        continue
    end
    
    % Check input is a number
    % str2num is more robust that str2double.
    if isempty(str2num(strInput))
        % Invalid input
        fprintf('Invalid input entered: %s \n',strInput);
        continue
    end
    % Trims any preceding spaces
    strInput = num2str(str2double(strInput));
    
    % Check input is a valid subject/week number
    if ~ismember(str2double(strInput),1:numLimit)
        % Invalid subject number
        fprintf('%s number not valid: %s \n', promptType, strInput);
        % Continue prompting for a valid number
        continue
    end
    
    % Check at most a 3-digit number has been entered. Catches
    % weird '0001' types of input.
    if length(strInput) > 3
        fprintf('Number must be 1, 2, or 3 digits long: %s\n', strInput);
        % Continue prompting for a valid number
        continue
    end
    
    % Prepend a zero to numbers less than 10, to ensure correct
    % ordering when dir contents listed. Also catch when operator
    % prepends a '0' to their week or subject number.
    if str2double(strInput) < 10 && ~isequal(strInput(1),'0')
        strInput = strcat('0', strInput);
    end
    
    numInput = str2double(strInput);
    
    %%%%
    % A valid numeric input has been entered to reach this line.
    % Break out of the loop, causing the function to return.
    break
end

end % end getValidInput