function usrconf = confirmInput(confMsg, confAllowed)
%CONFIRMINPUT	Loops until desired confirmation input is achieved
%   Loops until the desired confirmation command is supplied.

while true
    usrconf = lower(input(confMsg,'s'));
    if ismember(usrconf,confAllowed)
        break
    end
end

end % end confirmInput
