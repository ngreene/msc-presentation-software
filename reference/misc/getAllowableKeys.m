function [logkeys, logvals] = getAllowableKeys(responseOrder)
%GETALLOWABLEKEYS	Gets the set of keyboard codes accepted as response intpus
%
%   Keys are mapped with 1 always being neutral, 9 being none, 7 as disgust
%   (when speech) and 8 as surprised (when speech). This prevents a
%   changing of key input for participants when switching blocks between
%   speech and song.
%
%   Response order 1 => 1 neutral, 2 sad, 3 angry, 4 fearful, 5 calm,
%                       6 happy, 7 disgust, 8 surprised, 9 none
%                  2 => 1 neutral, 2 calm, 3 happy, 4, sad, 5 angry,
%                       6 fearful, 7 disgust, 8 surprised, 9 none
%

%	Author Steven R. Livingstone
%	Version 1.0
%   2013-03-02
%   Version 1.1
%   2013-08-02
%       Removed old unused code and comments

% 1 = Emotion category, 2 = Intensity (1:5), 3 = Genuine (1:2)
[logkeys, logvals] = deal(cell(2,1));

% For some reason, key '1' is keycode 30.
% Speech key feedback (Numeric keys 1:8)
logkeys{1} = [30:37 KbName('Space')];
% Song key feedback (Numeric keys 1:6)
logkeys{2} = [30:35 KbName('Space')];
% Intensity key feedback (Numeric keys 1:5)
logkeys{3} = 30:34;
% Genuineness key feedback (Numeric keys 1:5)
logkeys{4} = 30:34;

% Map logged values based on assigned subject response type
%
% Values are remapped to the RAVDESS filename numbering format:
% 	1 = Neutral, 2 = Calm, 3 = Happy, 4 = Sad, 5 = Angry, 6 = Fearful, 7 = Disgust, 8 = Surprised
if 2 == responseOrder
    % Numbering matches file name convention
    % N, C, H, Sa, A, F, D, Su, None
    logvals{1} = 1:length(logkeys{1});
    % N, C, H, Sa, A, F, None
    logvals{2} = [1:6 9];
elseif 1 == responseOrder
    % Reversed ordering for C,H,Sa,A,F
    % N, Sa, A, F, C, H, D, Su, None
    logvals{1} = [1 4 5 6 2 3 7 8 9];
    % N, Sa, A, F, C, H, None
    logvals{2} = [1 4 5 6 2 3 9];
else
    error('Bad responseOrder for subject\n');
end

% Intensity values are mapped 1 to 5 for all subjects
logvals{3} = 1:length(logkeys{3});

% Genuineness values are mapped 1 to 5 for all subjects
logvals{4} = 1:length(logkeys{4});

end % end function