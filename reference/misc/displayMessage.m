function displayMessage( win, messageStr, duration )
%DISPLAYMESSAGE	Display a message using psychtoolbox for duration
%   Shorthand for DrawFormattedText. Displays the string MESSAGESTR on
%   screen window WIN, for the time DURATION (in seconds). Handles flip
%   calls.

%	Copyright 2013
%	Author Steven R. Livingstone
%
%	Version 1.0
%	2013-09-03

% Display the string
DrawFormattedText(win, messageStr, 'center', 'center',[255 255 255],[],[],[],2);
% Flip text to screen
Screen('Flip', win);
% Pause for 3 seconds
WaitSecs(duration);
% Flip and blank the screen
Screen('Flip', win);

end

