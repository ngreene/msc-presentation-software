function showHalfwayMessage( win, messageStr )
%SHOWHALFWAYMESSAGE	Show halfway message, and pause until 'k' is pressed.
%
%   Show halfway message to user, then pause until the magic key 'k',
%   keycode 14, has been pressed.Used for test-retest RAVDESS validation.

%	Copyright 2014
%	Author Steven R. Livingstone
%
%	Version 1.0
%	2014-09-26

DrawFormattedText(win, messageStr, 'center', 'center',[255 255 255],[],[],[],2);
% Flip text to screen
Screen('Flip', win);

% Wait until magic key is pressed. That key is 'k'== 14
keyIndex = -1;

% Loop until an appropriate key has been pressed
while ~(ismember(keyIndex, 14))
    % Loop until allowable key (logkeys) has been pressed
    [~, keyCode] = KbWait([], 3);
    find(keyCode)
    % Locate non-zero keycode in returned vector (the key pressed)
    keyIndex = find(keyCode);
    % Reject multiple simultaneous key presses
    if length(keyIndex) ~= 1
        keyIndex = -1;
    end
end

% Update display
Screen('Flip', win);

end

