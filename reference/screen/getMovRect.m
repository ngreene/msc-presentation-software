function [movRect, screenCentre] = getMovRect(rect, movres, offset)
%GETMOVRECT   Get the rectangle coordinate to display movie file in.
%
%   RECT is output from Screen('OpenWindow') command. MOVRES is the 
%   resolution of the video file.
%   OFFSET is x,y centre offset for movie file on screen.

%   Copyright 2012
%	Author Steven R. Livingstone
%	Version 1.0
%   2012-12-04

if nargin < 3
    offset = [0 0];
end

% Center coordinates of our screen device
[screenCentre(1), screenCentre(2)] = RectCenter(rect);

% [xmin ymin xmax ymax] of our movie rectangle. Note, this can stretch
% videos.
%movRect = [(movres - screenCentre + offset) (2*movres - screenCentre + offset)];
movRect = [screenCentre(1)-movres(1)/2+offset(1) screenCentre(2)-movres(2)/2+offset(2) screenCentre(1)+movres(1)/2+offset(1) screenCentre(2)+movres(2)/2+offset(2)];

