function showInstructions(win, screenCentre, dispScreens)
%SHOWFEEDBACK	Presents instruction screens and wait for spacebar to progress
%
%   SHOWINSTRUCTIONS(WIN,SCREENCENTRE,DISPSCREENS).

%	Author Steven R. Livingstone
%	Version 1.0
%   2013-11-19

% Interupt keycodes
space = KbName('SPACE');

% XY of feedback image
imgsize = [1920 1080];

% Window size and position to show feedback images within (scales images)
destRect = [screenCentre(1)-imgsize(1)/2 screenCentre(2)-imgsize(2)/2 screenCentre(1)+imgsize(1)/2 screenCentre(2)+imgsize(2)/2];

% Show each instruction image, and await spacebar before continuing
for i = 1:length(dispScreens)
    % Make the texture
    itex = Screen('MakeTexture', win, imread(dispScreens{i}));
    % Show the instruction screen
    Screen('DrawTexture', win, itex, [], destRect);
    % Flip image to screen
    Screen('Flip', win);
    
    % Wait for spacebar to be pressed before continuing
    keyIndex = -1;
    
    % Loop until an appropriate key has been pressed
    while ~(ismember(keyIndex,space))
        % Loop until allowable key (logkeys) has been pressed
        [~, keyCode] = KbWait([], 3);
        % Locate non-zero keycode in returned vector (the key pressed)
        keyIndex = find(keyCode);
        % Reject multiple simultaneous key presses
        if length(keyIndex) ~= 1
            keyIndex = -1;
        end
    end
end

% Flip to clear screen
Screen('Flip', win);

end % end showInstructions