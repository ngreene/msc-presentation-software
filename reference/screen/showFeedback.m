function [keylog, keyIndexes, respTimes, abortProgram] = showFeedback(win, screenCentre, dispScreens, dispkeys, dispvals, abortProgram, stimType)
%SHOWFEEDBACK	Presents feedback screens and captures allowable keyboard responses
%
%   [KEYLOG,ABORTPROGRAM]=SHOWFEEDBACK(WIN,SCREENCENTRE,FEEDBACKIMGS,
%   LOGKEYS,LOGVALS,ABORTPROGRAM,STIMTYPE) blah.

%	Author Steven R. Livingstone
%	Version 1.0
%   2013-03-06
%   Version 1.1
%   2013-11-12
%       Additional key code information
%       Added stimType speech/song flag
%       Removed 'repeat' option

% Interupt keycodes
%esc = KbName('ESCAPE');
esc = KbName('DELETE');

% Mapped emotion & intensity responses
keylog = zeros(1,3);
% Response times for emotion & intensity responses
respTimes = zeros(1,3);
% Actual keyboard codes entered for emotion and intensity
keyIndexes = zeros(1,3);

% XY of feedback image
imgsize = [1920 1080];

% Window size and position to show feedback images within (scales images)
destRect = [screenCentre(1)-imgsize(1)/2 screenCentre(2)-imgsize(2)/2 screenCentre(1)+imgsize(1)/2 screenCentre(2)+imgsize(2)/2];

% Select key codes & images based on speech/song blocktype
if 1 == stimType
    % Speech. Drop song codes
    dispkeys(2) = [];
    dispvals(2) = [];
    dispScreens(2) = [];
elseif 2 == stimType
    % Song. Drop speech codes
    dispkeys(1) = [];
    dispvals(1) = [];
    dispScreens(1) = [];
else
    error('Invalid stimType: %d\n', stimType);
end

% Three feedback screens (category, intensity, genuineness)
for dispCnt = 1:3
    logkeys = dispkeys{dispCnt};
    logvals = dispvals{dispCnt};
    % Draw the texture (feedback image)
    Screen('DrawTexture', win, dispScreens{dispCnt}, [], destRect);
    % Flip image to screen
    Screen('Flip', win);
    timeOn = GetSecs;
    
    keyIndex = -1;
    % Loop until an appropriate key has been pressed
    while ~(ismember(keyIndex,[esc logkeys]))
        % Loop until allowable key (logkeys) has been pressed
        [~, keyCode] = KbWait([], 3);
        % Locate non-zero keycode in returned vector (the key pressed)
        keyIndex = find(keyCode);
        % Reject multiple simultaneous key presses
        if length(keyIndex) ~= 1
            keyIndex = -1;
        end
    end
    
    % Abort program if escape hit, else record valid rating
    if keyIndex == esc
        abortProgram = true;
        break
    else
        % Log key value pressed
        keylog(dispCnt) = logvals(ismember(logkeys,keyIndex));
        % Set response time
        respTimes(dispCnt) = GetSecs - timeOn;
        % Actual keyboard code entered (is not mapped)
        keyIndexes(dispCnt) = keyIndex;
    end
    
    % Wait for 150ms. Hopefully reduces people just constantly hitting the
    % one key.
    WaitSecs(0.15);
end

end % end showFeedback