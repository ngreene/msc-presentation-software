function [abortProgram] = playSampleTrials(logObj, win, movRect, pahandle, screenCentre, dispScreens, logkeys, logvals, abortProgram)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

for i = 1:3
    
    movieFilename = logObj.IV_sampleFiles{i};
    
    % Play example stimulus
    if i < 3
        % Prepare movie object
        movie = Screen('OpenMovie', win, movieFilename, 0, -1);
        
        % Play PTB-movie object
        playMovieTextures(win,movie,movRect);
        
        % Close movie object
        Screen('CloseMovie', movie);
        
    else
        % Load wav data
        wavdata = audioread(movieFilename)';
        
        % Buffer wav data
        PsychPortAudio('FillBuffer', pahandle, wavdata);
        
        % Play stimulus
        PsychPortAudio('Start', pahandle, 1, 0, 1);
        
        % Stop audio playback, but don't close handle object
        PsychPortAudio('Stop', pahandle, 1);
    end
    
    % Show feedback
    [~, ~, ~, abortProgram] = showFeedback(win, screenCentre, dispScreens, logkeys, logvals, abortProgram, logObj.IV_fileIDs(1,2));
    
    % Update display
    Screen('Flip', win);
end

end



function playMovieTextures(win, movie, movRect)
%PLAYMOVIETEXTURES	Display PTB-movie object's textures on screen.
%	Movie textures of MOVIE are displayed on WIN within the screen area 
%   MOVRECT.

Screen('PlayMovie', movie, 1);

while 1
    % Wait for next movie frame, retrieve texture handle to it
    tex = Screen('GetMovieImage', win, movie);
    
    % Valid texture returned?
    if tex < 0
        % Less than zero means movie is finished.
        break;
    end
    
    % Draw the new texture immediately to screen
    Screen('DrawTexture', win, tex, [], movRect);
    
    % Update display
    Screen('Flip', win);
    
    % Release texture
    Screen('Close', tex);
end

% Movie textures finished. Update display to show black screen.
Screen('Flip', win);

end
