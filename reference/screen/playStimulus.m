function movieFilename = playStimulus(logObj, dirs, win, movRect, pahandle)
%PLAYMOVIE	Plays speech or song timeline using PTB routines
%
%   PLAYSTIMULUS(WEEKLYOBJ,DIRS,WIN,MOVRECT) plays movie file (mp4) 
%   containedin directory DIR, with filename listed in WEEKLYOBJ. Movie 
%   textures are displayed on PTB-object WIN within the rectange area 
%   MOVRECT. 

%	Copyright 2013
%	Author Steven R. Livingstone
%
%	Version 1.0
%   2013-03-05
%       Two movie play functions (speech/song) with asycnhronous mp4 & wav
%
%   Version 1.1
%   2013-04-23
%       Single play function supporting only movie files (mp4, avi, etc).
%
%   Version 1.2
%   2013-08-02
%       Updated to return movie filename. Used for feedback correctness checks


% Get movie filename
movieFilename = fullfile(dirs.movie, logObj.IV_files{logObj.counter});

% Check file type
if logObj.IV_fileIDs(logObj.counter,1) < 3
    % Prepare movie object
    movie = Screen('OpenMovie', win, movieFilename, 0, -1);
    
    % Play PTB-movie object
    playMovieTextures(win,movie,movRect);
    
    % Close movie object
    Screen('CloseMovie', movie);
else
    % Load wav data
    wavdata = audioread(movieFilename)';

    % Buffer wav data
    PsychPortAudio('FillBuffer', pahandle, wavdata);

    % Play stimulus
    PsychPortAudio('Start', pahandle, 1, 0, 1);

    % Stop audio playback, but don't close handle object
    PsychPortAudio('Stop', pahandle, 1);
end

end % end playStimulus


function playMovieTextures(win, movie, movRect)
%PLAYMOVIETEXTURES	Display PTB-movie object's textures on screen.
%	Movie textures of MOVIE are displayed on WIN within the screen area 
%   MOVRECT.

Screen('PlayMovie', movie, 1);

while 1
    % Wait for next movie frame, retrieve texture handle to it
    tex = Screen('GetMovieImage', win, movie);
    
    % Valid texture returned?
    if tex < 0
        % Less than zero means movie is finished.
        break;
    end
    
    % Draw the new texture immediately to screen
    Screen('DrawTexture', win, tex, [], movRect);
    
    % Update display
    Screen('Flip', win);
    
    % Release texture
    Screen('Close', tex);
end

% Movie textures finished. Update display to show black screen.
Screen('Flip', win);

end
