#!/usr/bin/env python3

# loudnorm.py - Makes the audio amplitude of the inductions clips consistent.
#
# More specifically, this script uses ffmpeg to normalise according to 
# EBU R 128 (broadcast standard).
#
# https://bytesandbones.wordpress.com/2017/03/16/audio-nomalization-with-ffmpeg-using-loudnorm-ebur128-filter/

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

import os
import subprocess
import re

def get_values(text):
    ''' Extracts the relevant values from ffmpeg audio analysis output

    text -- the stderr output from ffmpeg
    '''
    # Get relevant output
    out = re.search('Input Integrated.+?$', text, re.DOTALL).group(0)

    # Extract numbers from relevant output
    values = re.findall('-?\d*\.{0,1}\d+', out, re.DOTALL)

    input_integrated = values[0]
    input_true_peak = values[1]
    input_lra = values[2]
    input_threshold = values[3]
    
    ''' sanity checks
    print(out)
    print('input integrated', input_integrated)
    print('input true peak', input_true_peak)
    print('input lra', input_lra)
    print('input threshold', input_threshold)
    '''

    return input_integrated, input_true_peak, input_lra, input_threshold

path = 'induction-clips/'       # The path to read files
out_path = 'normalised/'        # The path to stored audio normalised files
if not os.path.exists(out_path): os.makedirs(out_path)

for filename in os.listdir(path):
    if (filename.endswith('.mp4')):
        # Command for audio measurement with ffmpeg
        measure_command = ['ffmpeg', '-hide_banner', '-i', path+filename, '-af',
                   'loudnorm=I=-16:TP=-1.5:LRA=11:print_format=summary', '-f', 
                   'null', '-']

        # Store measurement command output
        result = subprocess.run(measure_command, stderr=subprocess.PIPE, 
                                stdout=subprocess.PIPE).stderr.decode('utf-8')


        # Get relevant values from audio measurement output
        in_int, in_tp, in_lra, in_thres = get_values(result)

        # A really long argument
        long_arg = str('loudnorm=I=-16:TP=-1.5:LRA=11:measured_I=' + in_int
                       + ':measured_TP=' + in_tp + ':measured_LRA=' + in_lra 
                       + ':measured_thresh=' + in_thres + ':offset=-0.7'
                       + ':linear=true:print_format=summary') 


        # Command for audio normalisation with ffmpeg
        norm_command = ['ffmpeg', '-i', path+filename, '-c:v', 'copy', '-af', 
                        long_arg, out_path+filename]

        # Run normalise command with measured values
        subprocess.run(norm_command)
    else:
        continue


