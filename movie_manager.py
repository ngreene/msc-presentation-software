#!/usr/bin/env python3

# movie_util.py - 

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

from psychopy import event, visual, core
from psychopy.iohub.client import launchHubServer

import event_marker 

io = launchHubServer()
kb = io.devices.keyboard

_skip_key = 'n'
_press_count = 0
_skip_enabled = False

def _enable_skip():
    '''Sets the boolean to skip the current playing clip to True when the skip
    key is pressed 3 times.
    '''
    global _skip_enabled 
    global _press_count

    _press_count += 1

    if _press_count >= 3:
        _skip_enabled = True
        _press_count = 0
event.globalKeys.add(key=_skip_key, func=_enable_skip)

# event to acqknowledge
_peak_exp_key = 'space'

class MovieManager():
    
    def __init__(self, win, clip_list):
        self.win = win
        self._load_clips(clip_list)
        
        # Clock times from the beginning of a trial that the strong experience
        # key was pressed. Resets to empty when a new trial begins.
        self.exp_times = [] 

    def _load_clips(self, clip_list):
        '''Creates and loads all the movie clips into the corresponding lists.

        emotive_clip_list -- a list of filenames for the emotive clips
        neutral_clip_list -- a list of filenames for the neutral clips
        '''
        self._clips = []
        count = 1
        loading_text = visual.TextStim(self.win, 
                                    text=('Loading movie files\n' + str(count) 
                                          + '/' + str(len(clip_list))), 
                                    pos=(0,0), 
                                    units='norm', 
                                    color='black', 
                                    wrapWidth=2)

        for filename in clip_list:
            loading_text.draw()
            self.win.flip()

            mov = visual.MovieStim3(self.win, 
                                    filename, 
                                    flipVert=False, 
                                    units='norm', 
                                    size=[2, 2]) 

            self._clips.append(mov)
            count += 1
            loading_text.text = ('Loading movie files\n' + str(count) + '/' 
                                 + str(len(clip_list)))

    def play_next_clip(self):
        '''Plays the next clip and logs strong experience key presses. 
        '''
        global _skip_enabled

        if not self._clips: return 

        mov = self._clips.pop(0)
        _skip_enabled = False
      
        self.exp_times = []
        kb.clearEvents()
        stime = core.getTime()
        event_marker.begin_trial()  # event to acqknowledge
        while mov.status != visual.FINISHED:

            if _skip_enabled: 
                mov.pause()
                break

            self.win.flip()
            mov.draw()

        event_marker.end_trial()    # event to acqknowledge
        self.last_clip_end_time = core.getTime()
        
        # Unfiltered key presses for strong experience
        raw_exp = []    
        for press in kb.getPresses():
            key = press.char
            if key == ' ': raw_exp.append(press.time - stime)

        # Filter out those key presses that occurred during the very beginning
        # of a playing clip and those that occurred too closely together in 
        # in time (as determined by the threshold)
        thres = 1.0 # threshold in seconds
        raw_exp = [value for value in raw_exp if value > thres]
        if not raw_exp: return  # no strong experience key presses occurred
        self.exp_times.append(raw_exp[0])
        for value in raw_exp:
            if abs(value - self.exp_times[-1]) > thres:
                self.exp_times.append(value)

