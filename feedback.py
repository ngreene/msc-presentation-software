#!/usr/bin/env python3

# feedback.py - Contains FeedbackScreen class which displays feedback screens 
# to participants and collects participant responses

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

# python standard
import os
import time

# psychopy
from psychopy import event, core
from psychopy.visual import RatingScale, TextStim, ImageStim

# The number of seconds before a response can be accepted
wait_time = 0.5 

# Defines the valid response keys to provide feedback (only works for row
# number keys not the number pad key)
response_keys = ['1', '2', '3', '4', '5', '6', '7']

# Dict remaps participant key presses to the emotion identifiers we use in 
# the dataset:
# 01 - neutral
# 02 - calm
# 03 - happy
# 04 - sad
# 05 - angry
# 06 - fearful
# 07 - disgust
discrete_emotion_remap = {
    "1" : "2",
    "2" : "3", 
    "3" : "4",
    "4" : "1",
    "5" : "5",
    "6" : "6",
    "7" : "7",
}

class FeedbackScreen:
    '''A class to handle the feedback screens presented after each trial

    Private attributes
    ----------
    win : Window
        The window for displaying stimuli
    text : TextStim
        The text displayed to the participant on the feedback screens
    emotion_sc : RatingScale
       Collects discrete emotion feedback
    valence_sc : RatingScale
        Collects valence feedback
    arousal_sc : RatingScale
        Collects arousal feedback
    keyboard_imgs_7 : List of ImageStim
        Images of number keys participants see to show valid feedback keys 
    keyboard_imgs_5 : List of ImageStim
        Same as keyboard_imgs_5 except with 5 keys
    glow_imgs_7 : List of ImageStim
        Images of transparent glow to show key selection; psychopy built in
        markers do not work with current setup
    glow_imgs_5 : List of ImageStim
        Same as glow_imgs_5 except with 5 keys
    grey_img : ImageStim
        A grey image to hide RatingScale marker
    '''

    def __init__(self, win):
        '''Initialises FeedbackScreen. Creates the three rating scales for 
        discrete emotion, valence, and arousal.
        '''
        self.win = win

        x_res = 2560 
        y_res = 1440

        # Discrete emotion background image
        self.emotion_bkg = ImageStim(win=win,
                                     image='img/discrete-emotion-bkg.png',
                                     units='pix',
                                     size=(x_res, y_res))

        # Valence background image          
        self.valence_bkg = ImageStim(win=win,
                                     image='img/valence-bkg.png',
                                     units='pix',
                                     size=(x_res, y_res))

        # Arousal background image
        self.arousal_bkg = ImageStim(win=win,
                                     image='img/arousal-bkg.png',
                                     units='pix',
                                     size=(x_res, y_res))

        # Intense experience background images
        self.intense_exp_bkg = ImageStim(win=win,
                                         image='img/intense-experience-bkg.png',
                                         units='pix',
                                         size=(x_res, y_res))

        stretch = 2.3
        size = 1
        text_size = 1

        # Discrete emotion rating scale
        self.emotion_scl = RatingScale(win,  
                                      choices=range(len(response_keys)), 
                                      respKeys=response_keys,
                                      size=size, 
                                      stretch=stretch, 
                                      textSize=text_size,
                                      noMouse=True, 
                                      pos=(0, 0), 
                                      markerStart=3,
                                      textColor='black',
                                      minTime=wait_time,
                                      leftKeys='',
                                      rightKeys='',
                                      marker=None,
                                      acceptKeys=response_keys,
                                      showAccept=False)

        # Valence rating scale
        self.valence_scl = RatingScale(win,  
                                      choices=range(len(response_keys)), 
                                      respKeys=response_keys,
                                      size=size, 
                                      stretch=(2/3)*stretch, 
                                      textSize=text_size,
                                      noMouse=True, 
                                      pos=(0, 0), 
                                      markerStart=3,
                                      textColor='black',
                                      minTime=wait_time,
                                      leftKeys='',
                                      rightKeys='',
                                      marker=None,
                                      acceptKeys=response_keys,
                                      showAccept=False)

        # Arousal rating scale
        self.arousal_scl = RatingScale(win,  
                                      choices=range(len(response_keys)), 
                                      respKeys=response_keys,
                                      size=size, 
                                      stretch=(2/3)*stretch, 
                                      textSize=text_size,
                                      noMouse=True, 
                                      pos=(0, 0), 
                                      markerStart=3,
                                      textColor='black',
                                      minTime=wait_time,
                                      leftKeys='',
                                      rightKeys='',
                                      marker=None,
                                      acceptKeys=response_keys,
                                      showAccept=False)

        # Intense experiece scale
        self.intense_exp_scl = RatingScale(win,
                                         choices=range(len(response_keys[:-4])),
                                         respKeys=response_keys[:-4],
                                         size=size,
                                         stretch=(1/3)*stretch,
                                         textSize=text_size,
                                         noMouse=True,
                                         pos=(0, 0),
                                         markerStart=2,
                                         textColor='black',
                                         minTime=wait_time,
                                         leftKeys='',
                                         rightKeys='',
                                         marker=None,
                                         acceptKeys=response_keys[:-4],
                                         showAccept=False)

        self.glow_imgs_7 = []
        self.glow_imgs_3 = []
        x_scale = 0.13
        y_scale = 1.6*x_scale
        key_space = 0.1*stretch*size
        y_pos = 0
        # find keyboard images and put them in the correct screen positions
        for i in range(len(response_keys)):
            x_pos = key_space*i - 3*key_space

            self.glow_imgs_7.append(ImageStim(win,
                                            image='img/glow.png',
                                            units='norm', 
                                            size=(2*x_scale, 2*y_scale),
                                            pos=(x_pos, y_pos)))


        for i in range(len(response_keys)-4):
            x_pos = key_space*i - key_space

            self.glow_imgs_3.append(ImageStim(win,
                                            image='img/glow.png',
                                            units='norm', 
                                            size=(2*x_scale, 2*y_scale),
                                            pos=(x_pos, y_pos)))

        self.count_txt = TextStim(win=self.win,
                                  text='',
                                  pos=(0.62, 0.30),
                                  color='black',
                                  units='norm')

    def get_feedback(self, key_presses): 
        '''Displays all feedback screens one after the other
            
        Returns
        -------
        A list with the first half containing the ratings, and the last half
        containing the decision times (seconds)
        '''
        def _display(bkg, scl, glow_imgs, count=0):
            scl.reset()
            if count > 0: self.count_txt.text = str(count)
            # While participant has not input feedback, display feedback screen
            while scl.noResponse:
                scl.draw() # Not seen
                bkg.draw() # Background draws over rating scale
                self.count_txt.draw()
                self.win.flip()

            rating = scl.getRating()
            rating_dt = scl.getRT()
            time_start = time.time()
            while time.time() < time_start + .5:
                bkg.draw()
                self.count_txt.draw()
                glow_imgs[rating].draw()
                self.win.flip()

            self.count_txt.text = ''
        
            return str(rating+1), str(rating_dt) # rating and decision time

        emotion_key, emotion_rating_dt = _display(self.emotion_bkg,
                                                   self.emotion_scl,
                                                   self.glow_imgs_7)
        # Remap to the discrete emotion representation we use in the dataset
        emotion_rating = discrete_emotion_remap[emotion_key]

        valence_rating, valence_rating_dt = _display(self.valence_bkg,
                                                     self.valence_scl,
                                                     self.glow_imgs_7)

        arousal_rating, arousal_rating_dt = _display(self.arousal_bkg, 
                                                     self.arousal_scl,
                                                     self.glow_imgs_7)

        intense_exp_ratings = []
        for i in range(key_presses):
            r, dt = _display(self.intense_exp_bkg,
                             self.intense_exp_scl,
                             self.glow_imgs_3,
                             i+1)
            intense_exp_ratings.append(r)

        return [emotion_rating, valence_rating, arousal_rating,
                intense_exp_ratings]

