#!/usr/bin/env python3

# experiment.py - An Experiment object serves as a clean interface to setup an
# experiment, play clips, save data, and close an experiment

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

import time
import os

from psychopy.visual import TextStim, Window, ImageStim
from psychopy.gui import Dlg
from psychopy import core, event

from movie_manager import MovieManager
from feedback import FeedbackScreen
from file_io import FileIO

_quit_key = 'q'
_quit_modifiers = ['ctrl', 'alt']
def _quit_experiment():
    '''Quits the experiment with key combination 'ctrl', 'alt/option', and 'q'
    '''
    core.quit()
event.globalKeys.add(key=_quit_key, modifiers=_quit_modifiers, 
                     func=_quit_experiment)

_continue_key = 'space'
_spacebar_pressed = False
def _continue():
    global _spacebar_pressed
    _spacebar_pressed = True
event.globalKeys.add(key=_continue_key, func=_continue)

class Experiment():

    def __init__(self):
        ''' Sets up and begins an experiment
        '''

        # Collect participant ID
        dlg = Dlg(title='Movie presentation experiment')
        dlg.addField('Participant ID:')
        participant_id_int = int(dlg.show()[0])
        participant_id = str(participant_id_int).zfill(2)
        print('participant_id', participant_id)
        if not dlg.OK: core.quit()
       
        self.win = Window(fullscr=True,
                           color='white')
        self._windows10_fix()

        self.f_io = FileIO(participant_id_int)
        self.feedback = FeedbackScreen(self.win)
        self.movie_manager = MovieManager(self.win, self.f_io.clip_list)

        self._pause_screen = ImageStim(win=self.win,
                                       image='img/pre-trial.png',
                                       units='pix',
                                       size=(2560, 1440))
     
        self._participant_id_text = TextStim(self.win, 
                                             text='ID: ' + participant_id, 
                                             pos=(-0.8, 0.9), 
                                             units='norm', 
                                             color='black', 
                                             wrapWidth=2)
        self._participant_id_text.size = 0.8

        self._run_practice_trial()


    def _run_practice_trial(self):
        info_files = [
            'img/info-slide-1.png',
            'img/info-slide-2.png',
            'img/info-slide-3.png',
            'img/info-slide-4.png',
            'img/info-slide-5.png',
            'img/info-slide-6.png',
            'img/info-slide-7.png',
            'img/info-slide-8.png'
        ]

        self.info_screen = ImageStim(win=self.win,
                                image=info_files.pop(0),
                                units='pix',
                                size=(2560, 1440))

        self._display_pause_screen()

        min_time = core.getTime() + 1.0

        while True:
            self._windows10_fix()
            self.info_screen.draw()
            self.win.flip()
            if event.getKeys(['space']) and core.getTime() > min_time: 
                if not info_files: break
                self.info_screen.image = info_files.pop(0) 
                min_time = core.getTime() + 1.0

        self.movie_manager.play_next_clip() # play training clip
        self.feedback.get_feedback(1)

        self.info_screen.image = 'img/info-slide-9.png'
        min_time = core.getTime() + 1.0
        while True:
            self._windows10_fix()
            self.info_screen.draw()
            self.win.flip()
            if event.getKeys(['space']) and core.getTime() > min_time: 
                return
    # end _run_participant_training


    
    def _windows10_fix(self):
        self.win.mouseVisible = False
        self.win.winHandle.activate()

    def ended(self):
        return not self.movie_manager._clips

    def _display_pause_screen(self):
        global _spacebar_pressed
        _spacebar_pressed = False

        while not _spacebar_pressed:
            self._windows10_fix()
            #self._pause_text.draw()
            self._pause_screen.draw()
            self._participant_id_text.draw()
            self.win.flip()
        
    def play_next_clip(self):
        self._display_pause_screen()
        self.movie_manager.play_next_clip()

    def collect_feedback(self):
        exp_times = self.movie_manager.exp_times
        ratings = self.feedback.get_feedback(len(exp_times))
        emotion_rating = ratings[0]
        valence_rating = ratings[1]
        arousal_rating = ratings[2]
        exp_ratings = ratings[3]

        # interleave two lists into one like so: 
        # [rating1, rating1_time, ..., ratingN, ratingN_time]
        strong_exp = [value for pair in zip(exp_ratings, exp_times) 
                      for value in pair]

        self.f_io.save_feedback(emotion_rating, 
                                valence_rating, 
                                arousal_rating,
                                strong_exp)

    def close(self):
        global _spacebar_pressed

        self.info_screen.image = 'img/breathing-1.png'
        breathe_start = core.getTime()
        while core.getTime() < breathe_start + 10:
            self.info_screen.draw()
            self.win.flip()

        self.info_screen.image = 'img/breathing-2.png'
        _spacebar_pressed = False
        event.clearEvents()
        
        while not _spacebar_pressed:
            self.info_screen.draw()
            self.win.flip()
        breathe_end = core.getTime()
        
        breathe_start -= self.movie_manager.last_clip_end_time
        breathe_end -= self.movie_manager.last_clip_end_time
        
        self.f_io.write_file('breathing_times.csv', 
                             'time_from_last_trial,total_time\n' 
                             + str(breathe_start) + ',' 
                             + str(breathe_end-breathe_start) + '\n')

        self.info_screen.image = 'img/end.png'
        _spacebar_pressed = False
        while not _spacebar_pressed:
            self.info_screen.draw()
            self.win.flip()

        self.win.close()
        core.quit()

