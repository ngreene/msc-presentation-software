#!/usr/bin/env python3

# main.py - Run this script to begin an experiment

__author__ = 'Nicholas Greene'
__copyright__ = 'Copyright 2021 Nicholas Greene'
__license__ = 'MIT'
__version__ = '1.0'

# psychopy 
from psychopy import logging

# my files
from experiment import Experiment

logging.console.setLevel(logging.CRITICAL) # stops psychopy console warnings

experiment = Experiment()
while not experiment.ended(): 
    experiment.play_next_clip()   # present movie clip
    experiment.collect_feedback() # present feedback screen

experiment.close()  # Save all and close experiment

